from app import db
from app import app
from app import app_redis
from app.api import BaseCcontroller
from app.models.UserModel import UserModel
from app.helpers.make_code import make_code

from flask import request
from flask import url_for
import json


class ConfirmController(BaseCcontroller):
    route_base = '/confirm/'

    def index(self):
        code = request.args.get('code')

        data = app_redis.get('confirm-'+code)

        if data:
            data = json.loads(data.decode())
            code = make_code()
            user = UserModel(**data)

            db.session.add(user)
            db.session.commit()

            self.check_invite(user.id)

            app_redis.set('session-'+code, user.id)
            app_redis.delete('confirm-'+code)

            self.set_cookie(key="uid", value=code)
            return self.redirect(url_for('HomeController:index'))

        return self.render('pages/confirm.html')

    def check_invite(self, children_user):
        invite = request.cookies.get('_invite')

        if not invite:
            return None

        user = UserModel.query.filter(
            UserModel.invite == invite
        ).first()

        if not user:
            return None

        children = user.children or []
        children.append(children_user)

        user.children = [item for item in children]

        user.parent = invite
        db.session.add(user)
        db.session.commit()


ConfirmController.register(app)
