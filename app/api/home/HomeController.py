from app import app
from app.api import BaseCcontroller
from app.api import authenticate

from app.states import USER_STATUS_GRAND_PARTNER
from app.states import USER_STATUS_MINER

from app.models.UserModel import UserModel
from flask import request


class HomeController(BaseCcontroller):
    route_base = '/'

    @authenticate
    def index(self):
        status = self.current_user().status
        pool = request.args.get('pool')
        address = []

        if pool and status != USER_STATUS_MINER:
            users = []

            for item in UserModel.query.filter(
                UserModel.id.in_(self.current_user().children or [])
            ).all():
                address.append(item)
                for user_id in item.children:
                    users.append(user_id)

            if status == USER_STATUS_GRAND_PARTNER:
                for item in UserModel.query.filter(
                        UserModel.id.in_(users)
                ).all():
                    address.append(item)

        if status == USER_STATUS_MINER:
            user = self.current_user()
            if user.purse_type and user.purse:
                pool = user.purse_type
                address = [user]

        return self.render(
            'pages/home.html',
            pools=self.api_get_pool()['pools'],
            statistic=self.agregate_address(address, pool)
        )

    def agregate_address(self, address, pool):
        data = []

        for item in address:
            if pool == item.purse_type and item.purse:
                data.append(
                    {
                        'user': item,
                        'statistic': self.api_get_statistic(pool, item.purse)
                    }
                )

        return data


HomeController.register(app)
