from app import db
from app import app
from app.api import BaseCcontroller
from app.api import authenticate

from .forms import SettingsForm
from .forms import ChangePasswordForm

from werkzeug.security import generate_password_hash
from flask import request


class SettingsController(BaseCcontroller):
    route_base = '/settings/'

    @authenticate
    def index(self):
        form = SettingsForm(request.form)
        form_password = ChangePasswordForm(request.form)
        data = self.api_get_pool()
        if request.method == 'POST':
            user = self.current_user()

            if form_password.old_password.data and form_password.validate():
                if user.check_password(form_password.old_password.data):
                    user.password = generate_password_hash(form_password.password.data)

                    db.session.commit()

                else:
                    pass

                form.set_values(self.current_user().__dict__)

            elif form.validate():
                user.email = form.email.data
                user.first_name = form.first_name.data
                user.last_name = form.last_name.data
                user.organization = form.organization.data
                user.phone = form.phone.data
                user.purse = form.purse.data
                user.purse_type = form.purse_type.data

                db.session.commit()

        else:
            form.set_values(self.current_user().__dict__)

        return self.render('pages/settings.html', pools=data['pools'], form=form, form_password=form_password)

    def post(self):
        return self.index()


SettingsController.register(app)

