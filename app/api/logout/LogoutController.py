from app import app
from app import app_redis
from app.api import BaseCcontroller

from flask import request
from flask import url_for


class LogoutController(BaseCcontroller):
    route_base = '/logout/'

    def index(self):
        code = request.cookies.get('uid')
        if code:
            app_redis.set('session-' + code, 0)
            app_redis.delete('session-' + code)

        return self.redirect(url_for('LoginController:index'))


LogoutController.register(app)