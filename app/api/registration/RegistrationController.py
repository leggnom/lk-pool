from app import app
from app import app_redis
from app.api import BaseCcontroller
from app.helpers.send_mail import send_mail
from app.helpers.make_code import make_code

from .forms import RegistrationForm

from werkzeug.security import generate_password_hash
from flask import request

import json


class RegistrationController(BaseCcontroller):
    route_base = '/registration/'

    def index(self):
        form = RegistrationForm(request.form)
        is_saved = False

        if request.method == 'POST' and form.validate():
            code = make_code()

            data = form.data
            data['password'] = generate_password_hash(password=form.password.data)
            del data['confirm']

            app_redis.set(
                'confirm-' + code,
                json.dumps(data),
                ex=app.config['EXPIRE_CONFIRM']
            )

            self._send_message(code, form.email.data)
            is_saved = True

        return self.render('pages/registration.html', form=form, is_saved=is_saved)

    def post(self):
        return self.index()

    def _send_message(self, code, email):
        message = "Для подтверждения регистрации пройдите по ссылке {}confirm/?code={}".format(
            app.config['SITE_URL'],
            code
        )

        send_mail(message, "Подтверждение регистрации", app.config['SMTP_FROM'], email)


RegistrationController.register(app)
