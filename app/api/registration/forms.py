from wtforms import Form
from wtforms import validators
from wtforms import StringField
from wtforms import IntegerField
from wtforms import PasswordField


class RegistrationForm(Form):
    status = IntegerField('')
    first_name = StringField('')
    last_name = StringField('')

    organization = StringField('')
    phone = StringField('', validators=[
        validators.Regexp(
            '(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})',
            0,
            message="Недопустимое значение"
        ),
    ])

    email = StringField('', validators=[
        validators.required(),
        validators.Email(message="Недопустимое значение"),
    ])
    password = PasswordField('', validators=[
        validators.required(),
        validators.Length(min=6, max=20, message="Не меньше 6 символов и не больше 20")
    ])
    confirm = PasswordField('', [
        validators.required(),
        validators.EqualTo('password', message='Это поле должно соответствовать полю "Пароль"')
    ])
