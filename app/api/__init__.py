from app import app
from app import app_redis

from app.models.UserModel import UserModel

from functools import wraps
from flask_classy import FlaskView

from flask import render_template
from flask import make_response
from flask import request
from flask import redirect
from flask import url_for

from urllib.parse import urljoin

import requests
import json
import re


REGEXP_URL = re.compile('\-([0-9]+)\/$')


class BaseCcontroller(FlaskView):
    _user = None

    authenticated = False

    mimtype = 'application/json; charset=utf-8'
    cookie = []

    def before_request(self, name):
        code = request.cookies.get('uid')
        self._user = None
        self.authenticated = False

        if code:
            uid = app_redis.get('session-' + code)

            if uid:
                uid = self.to_int(uid)

            if uid:
                user = UserModel.query.filter(
                    UserModel.id == uid
                ).first()

                if user:
                    self._user = user
                    self.authenticated = True


    def current_user(self):
        return self._user

    def render(self, path, **kwargs):
        kwargs.update(
            dict(
                current_user=self.current_user(),
            )
        )
        return render_template(path, **kwargs)

    def response(self, **kwargs):
        result = dict(success=0)
        result.update(kwargs)

        resp = make_response(json.dumps(result), 200)

        resp.mimetype = self.mimtype

        for item in self.cookie:
            resp.set_cookie(**item.copy())

        self.cookie = []

        return resp

    def set_cookie(self, **kwargs):
        self.cookie.append(kwargs)

    def redirect(self, url):
        resp = make_response(redirect(url))
        for item in self.cookie:
            resp.set_cookie(**item.copy())
        return resp

    def to_int(self, value='', default=0):
        value = str(value) if value is None else value
        try:
            return int(value)
        except ValueError:
            return default

    def api_get_pool(self):
        r = requests.get(
            urljoin(app.config['API_HOST'], '/ap/pools')
        )

        return r.json()

    def api_get_statistic(self, poolid, address):
        r = requests.get(
            urljoin(app.config['API_HOST'], '/ap/pools/{}/miners/{}'.format(poolid, address))
        )
        return r.json()


def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        _self = args[0] if len(args) else None
        user = False

        if not getattr(func, 'authenticated', True):
            return func(*args, **kwargs)

        if hasattr(_self, 'current_user'):
            user = _self.current_user()

        if user:
            return func(*args, **kwargs)

        return redirect(url_for('LoginController:index'))
    return wrapper
