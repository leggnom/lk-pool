from app import app
from app import app_redis
from app.api import BaseCcontroller
from app.models.UserModel import UserModel
from app.helpers.make_code import make_code

from .forms import LoginForm

from werkzeug.security import generate_password_hash
from flask import redirect
from flask import request
from flask import url_for


class LoginController(BaseCcontroller):
    route_base = '/login/'

    def index(self):
        if self.current_user():
            return redirect(url_for('HomeController:index'))

        user_not_found = False
        form = LoginForm(request.form)

        if request.method == 'POST' and form.validate():
            code = make_code()

            user = UserModel.query.filter(
                UserModel.email == form.email.data
            ).first()

            if user and user.check_password(form.password.data):
                app_redis.set('session-' + code, user.id)
                self.set_cookie(key='uid', value=code)
                return self.redirect(url_for('HomeController:index'))

            user_not_found = True

        return self.render('pages/login.html', user_not_found=user_not_found, form=form)

    def post(self):
        return self.index()


LoginController.register(app)