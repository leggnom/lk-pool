from wtforms import Form
from wtforms import validators
from wtforms import StringField
from wtforms import PasswordField


class LoginForm(Form):
    email = StringField('', validators=[
        validators.required(),
        validators.Email(message="Недопустимое значение"),
    ])
    password = PasswordField('', validators=[
        validators.required(),
        validators.Length(min=6, max=20, message="Не меньше 6 символов и не больше 20")
    ])
