from app import db
from app import admin
from app.models.UserModel import UserModel

from flask_admin.contrib.sqla import ModelView

from wtforms import StringField
from wtforms import FloatField
from wtforms import Form


class UserAdminForm(Form):
    email = StringField()
    first_name = StringField()
    last_name = StringField()
    organization = StringField()
    pay = FloatField(default=0)


class UserModelView(ModelView):
    can_create = False
    edit_template = "edit-user.html"

    column_filters = ("first_name", "last_name", "organization", "purse_type", "invite")
    column_list = ("email", "first_name", "last_name", "organization", "purse_type", "create_date")
    form = UserAdminForm

    # def edit_form(self, obj=None):
    #     return self.edit_form(obj)

    def get_detail_value(self, context, model, name):
        print(context)
        return super(UserAdminForm).get_detail_value(
            context,
            model,
            name
        )


admin.add_view(UserModelView(UserModel, db.session))
