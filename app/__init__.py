import redis

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask import request


app = Flask(__name__)

app.config.from_object('config.default')
app.config.from_pyfile('../config/development.py', True)
app.config.from_pyfile('/var/www/config.py', True)

db = SQLAlchemy(app)
admin = Admin(app, name='API', template_mode='bootstrap3')

app_redis = redis.StrictRedis(
    host=app.config['REDIS']['HOST'],
    port=app.config['REDIS']['PORT'],
    db=app.config['REDIS']['DB']
)


"""
    Models
"""
from app.models import *


"""
    Routes
"""
from app.api.login import *
from app.api.logout import *
from app.api.registration import *
from app.api.confirm import *
from app.api.home import *
from app.api.settings import *

from app.user_admin_view import *


@app.after_request
def before_request_handler(r):
    invite = request.args.get('invite')

    if invite:
        r.set_cookie('_invite', invite)

    return r

