import hashlib
import random
import datetime


def create_invite():
    m = hashlib.md5()
    m.update(
        'hash-{}-{}'.format(
            str(datetime.datetime.now()),
            random.randint(0, 100)
        ).encode('utf-8')
    )
    return m.hexdigest()
