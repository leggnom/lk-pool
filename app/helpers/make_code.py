import random


def make_code(count=128):
    return 'hs{}'.format(random.getrandbits(count))
