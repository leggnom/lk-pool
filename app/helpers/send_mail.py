from app import app
import smtplib


def send_mail(text, subject, _from, _to):
    server = smtplib.SMTP_SSL(app.config['SMTP_HOST'], app.config['SMTP_PORT'])
    server.ehlo()

    if app.config['SMTP_LOGIN'] and app.config['SMTP_PASSWORD']:
        server.login(app.config['SMTP_LOGIN'], app.config['SMTP_PASSWORD'])

    message = "\r\n".join([
        "From: {}".format(_from),
        "To: {}".format(_to),
        "Subject: {}".format(subject),
        "",
        str(text)
    ])

    server.sendmail(_from, _to, message.encode('utf-8'))
    server.quit()
