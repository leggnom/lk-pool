from app import db
from app.states import USER_STATUS_PARTNER
from app.helpers.create_invite import create_invite

from werkzeug.security import check_password_hash
import datetime


class UserModel(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)

    status = db.Column(db.Integer, default=USER_STATUS_PARTNER)

    # пользователь который пригласил
    parent = db.Column(db.String)
    children = db.Column(db.ARRAY(db.Integer), default=[])

    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    organization = db.Column(db.String)

    invite = db.Column(db.String, default=create_invite)

    purse = db.Column(db.String)
    purse_type = db.Column(db.String)

    email = db.Column(db.String, unique=True)
    phone = db.Column(db.String)
    password = db.Column(db.String)

    create_date = db.Column(db.DateTime, default=datetime.datetime.now)

    def check_password(self, passwd):
        return check_password_hash(self.password, passwd)

    def reset_invite(self):
        self.invite = create_invite()
        db.session.commit()
